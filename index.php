<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once('database.php');
$db = new db('localhost','root','root','events');
if(isset($_POST["import"])){
  $jsondata = file_get_contents($_FILES['events']['tmp_name']);
  $events = json_decode($jsondata, true);
    foreach($events as $event){
      $event_id = $event['event_id'];
      $employee_data = $db->query('SELECT id FROM employees WHERE  employee_email = ?',$event['employee_mail'])->fetchArray();
      if(empty($employee_data)){
        $db->query('INSERT INTO employees (employee_name,employee_email) values(?,?) ',$event['employee_name'], $event['employee_mail']);
        $empolyee_id = $db->lastInsertID();
      }else{
        $empolyee_id = $employee_data['id'];
      }

      $events_data = $db->query('SELECT id FROM events WHERE id = ?',$event['event_id'])->fetchArray();
      if(empty($events_data)){
        $events_data =  $db->query('INSERT INTO events (event_name,event_date) values(?,?)',$event['event_name'] ,$event['event_date']);
        $event_id == $db->lastInsertID();
      }
      $paticipant_data = $db->query('SELECT id FROM participants_event_mapping WHERE participant_id = ? AND event_id = ? AND  employee_id =? ',$event['participation_id'],$event_id,$empolyee_id)->fetchArray();
      if(empty($paticipant_data)){
        $db->query('INSERT INTO participants_event_mapping (participant_id,event_id,employee_id,participation_fees) values(?,?,?,?)',$event['participation_id'] ,$event_id,$empolyee_id,$event['participation_fee']);
      }
    }
}
$events_list = array();
$sql = "SELECT m.participation_fees,m.participant_id,e.event_name,e.event_date,em.employee_name,em.employee_email FROM participants_event_mapping m
INNER JOIN EVENTS e ON e.id = m.`event_id`
INNER JOIN  employees em ON em.id = m.`employee_id`";

if(isset($_POST["filter"])){
  $emp = htmlspecialchars($_POST['emp_name']);
  $event = htmlspecialchars($_POST['event_name']);
  $date = htmlspecialchars($_POST['event_date']);
  $where_clause = " WHERE em.employee_name LIKE '%$emp%' AND e.event_name LIKE '%$event%' ";
  $where_clause.= !empty($date) ? " AND e.event_date=$date" : '';
  $db->query($sql.$where_clause)->fetchAll(function ($item) use(&$events_list) {
    array_push($events_list,$item);
  });
}else{
  $db->query($sql)->fetchAll(function ($item) use(&$events_list) {
    array_push($events_list,$item);
  });
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Events Booking</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<!-- <script>
  $('.input-daterange input').each(function() {
    $(this).datepicker('clearDates');
});
</script> -->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid ">    
  <div class="row content ">
    <div class="col-sm-12 "> 
      <h1>Import Events</h1>
          <form post="index.php" method="post" enctype="multipart/form-data">
              <div class="form-group" >
                <label for="exampleFormControlFile1">Select Json file</label>
                <input type="file" name="events" class="form-control-file" id="exampleFormControlFile1">
                <br>
                <button type="submit" name="import" class="btn btn-primary">Run Imports</button>
              </div>
          </form>
<h1>Events Listings</h1>
<form method="post">
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Empolyee Name</label>
      <input type="text" class="form-control mb-2" name="emp_name" id="inlineFormInput" placeholder=<?php  echo !empty($_POST['emp_name']) ? $_POST['emp_name'] :"Empolyee Name" ?>>
    </div>
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Event Name</label>
      <input type="text" class="form-control mb-2" name="event_name" id="inlineFormInput" placeholder = <?php echo !empty($_POST['event_name']) ? $_POST['event_name'] :"Event Name" ?>>
    </div>
    <input type="date" id="eventdata" name="event_date">
    <div class="col-auto">
      <button type="submit" name="filter" class="btn btn-primary mb-2">Submit</button>
    </div>
  </div>
</form>
<table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Emplyee Name</th>
      <th scope="col">Emplyee Email</th>
      <th scope="col">Event Name</th>
      <th scope="col">Event Date</th>
      <th scope="col">Participation Fees</th>
      <th scope="col">Participation ID</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $count = 1;
    $total = 0;
    foreach($events_list as $event_record){
      $total += $event_record['participation_fees'];
      ?>
    <tr>
        <th scope="row"><?php echo $count?></th>
        <td><?php echo $event_record['employee_name'] ?></td>
        <td><?php echo $event_record['employee_email']?></td>
        <td><?php echo $event_record['event_name']?></td>
        <td><?php echo $event_record['event_date']?></td>
        <td><?php echo $event_record['participation_fees']?></td>
        <td><?php echo $event_record['participant_id']?></td>
      </tr>
    
    <?php $count ++; }  if(isset($_POST["filter"])){ ?>
    <tr>
        <th scope="row">total fees</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?php echo $total?></td>
      </tr>
      <?php } ?>
  </tbody>
</table>
</div>
</div>
</body>
</html>
